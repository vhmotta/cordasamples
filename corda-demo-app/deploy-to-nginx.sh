#!/bin/bash

# $1 = nome de usuario no servidor
# $2 = end da chave privada
BASEDIR=$(dirname "$0")

cd ${BASEDIR}

tar -cjvf www.tar.bz2 www

scp -i $2 www.tar.bz2 $1@corda.bluchain.tech:/tmp/

rm -rf www.tar.bz2

# entrar em /bluchain/democorda/docker/nginx
#executar:
#./update.sh vivo
#ou
#./update.sh semparar
