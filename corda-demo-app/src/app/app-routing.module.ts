import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'onboarding',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'onboarding', loadChildren: './pages/onboarding/onboarding.module#OnboardingPageModule' },
  { path: 'wallet', loadChildren: './pages/wallet/wallet.module#WalletPageModule' },
  { path: 'request', loadChildren: './pages/request/request.module#RequestPageModule' },
  { path: 'qr-reader', loadChildren: './pages/modals/qr-reader/qr-reader.module#QrReaderPageModule' },
  { path: 'admin', loadChildren: './pages/admin/admin.module#AdminPageModule' },
  { path: 'history', loadChildren: './pages/history/history.module#HistoryPageModule' },
  { path: 'organization', loadChildren: './pages/organization/organization.module#OrganizationPageModule' },
  { path: 'transfer', loadChildren: './pages/transfer/transfer.module#TransferPageModule' },
  { path: 'clear', loadChildren: './pages/clear/clear.module#ClearPageModule' },
  { path: 'payment', loadChildren: './payment/payment.module#PaymentPageModule' },
  { path: 'create-charge', loadChildren: './pages/create-charge/create-charge.module#CreateChargePageModule' },
  { path: 'charges', loadChildren: './pages/see-charges/see-charges.module#SeeChargesPageModule' },
  { path: 'receive-charges', loadChildren: './pages/receive-charges/receive-charges.module#ReceiveChargesPageModule' },
  { path: 'payment-no-barcode', loadChildren: './pages/payment-no-barcode/payment-no-barcode.module#PaymentNoBarcodePageModule' },
  { path: 'recharge-cash', loadChildren: './pages/recharge-cash/recharge-cash.module#RechargeCashPageModule' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

