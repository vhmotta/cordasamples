export interface Organization {
  name: string;
  org: string;
  port: number;
}
