export interface Transaction {
  'state': {
    'data': {
      'transfer': {
        'orgFrom': 'O=OrgA, L=São Paulo, C=BR',
        'accountFrom': '8a7e46f6-8ca5-452e-9c3c-03662849a695',
        'accountFromName': 'vitor',
        'orgTo': 'O=OrgB, L=Campinas, C=BR',
        'accountTo': '4aa359cb-6b07-4f83-bec6-b0d4e6b11fd1',
        'accountToName': 'joao',
        'createTime': '2019-02-21T12:38:31.413Z',
        'amount': 100, 'status': 'SHARED'},
        'linearId': {
          'externalId': null,
          'id': '8ea9081b-cfed-4c49-974a-14711c15c4a3'
        },
        'participants': [
          'O=OrgA, L=São Paulo, C=BR',
          'O=OrgB, L=Campinas, C=BR'
        ]
      },
      'contract': 'tech.bluchain.democordanodes.contract.TransferContract',
      'notary': 'O=Notary, L=Campinas, C=BR',
      'encumbrance': null,
      'constraint': {
        'attachmentId': '98277E6CCA1334E820D9D91B35E33BB6CFA45D86F811E8A0113873E89E4C845F'
      }
    };
    'ref': {
      'txhash': 'D64797B97AAFEB5515B0E617AAB29D82EFE270A4AF801D64292BCDABE169348D',
      'index': 1
    };
}
