export interface Qrcode {
  recipient: string;
  amount: number;
  recOrg: string;
}

export interface TransactionRequest {
  accountTo: string;
  amount: string;
  orgTo: string;
  orgToName: string;
  accountToName: string;
}
