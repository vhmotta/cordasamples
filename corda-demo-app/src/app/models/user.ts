export interface User {
  state: {
    data: {
      account: {
        org: string;
        orgName: string;
        createTime: string;
        name: string;
        balance: string;
      };
      linearId: {
        id: string;
      };
      participants: string[]
    };
  };
  ref: {
    txhash: string;
    index: number;
  };
}
