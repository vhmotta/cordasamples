import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CordaService } from './providers/corda/corda.service';
import { ComponentsModule } from './components/components.module';
import { QrReaderPageModule } from './pages/modals/qr-reader/qr-reader.module';
import { PayChargePageModule } from './pages/modals/pay-charge/pay-charge.module';
import { PaymentPageModule } from './payment/payment.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: 'md',
    }),
    AppRoutingModule,
    ComponentsModule,
    QrReaderPageModule,
    PayChargePageModule,
    PaymentPageModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    CordaService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
