import { Component, OnInit } from '@angular/core';
import { CordaService } from 'src/app/providers/corda/corda.service';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  user;
  moment = moment;
  orgs = environment.orgList;

  transferList = [];
  constructor(public corda: CordaService) {
    this.user = this.getUser();
  }

  ngOnInit() {
    this.getHistory();
    // MOCK data
    // this.getMockHitory();

    /*
    this.corda.getAllTransfer().then((transferList: Array<any>) => {
      transferList.forEach(transfer => {
        if (transfer.state.data.transfer.accountFrom === this.user.linearId
          || transfer.state.data.transfer.accountTo === this.user.linearId) {
          // TODO: Alterar os nomes para empresas da demonstração
          if (transfer.state.data.transfer.orgFrom === 'O=OrgB, L=Campinas, C=BR') {
            transfer.state.data.transfer.orgFrom = 'Vivo';
          }
          if (transfer.state.data.transfer.orgFrom === 'O=OrgA, L=São Paulo, C=BR') {
            transfer.state.data.transfer.orgFrom = 'Sem Parar';
          }
          if (transfer.state.data.transfer.orgTo === 'O=OrgB, L=Campinas, C=BR') {
            transfer.state.data.transfer.orgTo = 'Vivo';
          }
          if (transfer.state.data.transfer.orgTo === 'O=OrgA, L=São Paulo, C=BR') {
            transfer.state.data.transfer.orgTo = 'Sem Parar';
          }
          this.transferList.push(transfer);
        }
      });
      console.log(transferList);

      console.log(this.transferList);
    });
    */

  }

  ionViewDidEnter() {
  }


  getHistory() {
    this.corda.getHistory(this.user.linearId).then( (history: Array<any>) => {
      this.transferList = history;
      console.log(history);
    });
  }

  getUser() {
    return JSON.parse(localStorage.getItem('cordaUser.'));
  }

  toggleInfo(transfer) {
    transfer.info = !transfer.info;
  }

  getMockHitory() {
    this.transferList = [
      {
        info: false,
        type: 'transfer',
        'state':
        {
          'data':
          {
            'transfer':
            {
              'orgFrom': 'O=OrgA, L=São Paulo, C=BR',
              'accountFrom': 'd4ae05d7-9f5f-4d5f-b54d-31a9d3750d30',
              'accountFromName': 'Juvenal',
              'orgTo': 'O=OrgB, L=Campinas, C=BR',
              'accountTo': '4aa359cb-6b07-4f83-bec6-b0d4e6b11fd1',
              'accountToName': 'joao',
              'createTime': '2019-02-21T12:38:31.413Z',
              'amount': 100,
              'status': 'SHARED'
            },
            'linearId': {
              'externalId': null,
              'id': '8ea9081b-cfed-4c49-974a-14711c15c4a3'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgB, L=Campinas, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.TransferContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '98277E6CCA1334E820D9D91B35E33BB6CFA45D86F811E8A0113873E89E4C845F'
          }
        },
        'ref': {
          'txhash': 'D64797B97AAFEB5515B0E617AAB29D82EFE270A4AF801D64292BCDABE169348D',
          'index': 1
        }
      },
      {
        info: false,
        type: 'transfer',
        'state':
        {
          'data':
          {
            'transfer':
            {
              'orgFrom': 'O=OrgA, L=São Paulo, C=BR',
              'accountFrom': '4aa359cb-6b07-4f83-bec6-b0d4e6b11fd1',
              'accountFromName': 'joao',
              'orgTo': 'O=OrgB, L=Campinas, C=BR',
              'accountTo': '8a7e46f6-8ca5-452e-9c3c-03662849a695',
              'accountToName': 'Juvenal',
              'createTime': '2019-02-21T12:38:31.413Z',
              'amount': 100,
              'status': 'SHARED'
            },
            'linearId': {
              'externalId': null,
              'id': '8ea9081b-cfed-4c49-974a-14711c15c4a3'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgB, L=Campinas, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.TransferContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '98277E6CCA1334E820D9D91B35E33BB6CFA45D86F811E8A0113873E89E4C845F'
          }
        },
        'ref': {
          'txhash': 'D64797B97AAFEB5515B0E617AAB29D82EFE270A4AF801D64292BCDABE169348D',
          'index': 1
        }
      },
      {
        info: false,
        type: 'payment',
        'state': {
          'data': {
            'paymentSlip': {
              'paymentOrg': 'O=OrgA, L=São Paulo, C=BR',
              'code': '1234567890',
              'account': '4a543324-2737-4a03-b244-5d6f6c148d78',
              'createTime': '2019-02-27T20:13:21.228Z',
              'amount': 5.0
            },
            'linearId': {
              'externalId': null,
              'id': '9ecc2a17-86f9-49d9-86f7-f9e3252c4ace'
            },
            'participants': ['O=OrgA, L=São Paulo, C=BR']
          },
          'contract': 'tech.bluchain.democordanodes.contract.PaymentSlipContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '7280DBFA51F6E605D87414CA123D48F3CE44B24DAEC1291BC14DC1317E4F7BED'
          }
        },
        'ref': {
          'txhash': '684399861DF75D3DD23A17326B80B6741C6EE68B5C61EB4B0FBD1F4414E20A1B',
          'index': 0
        }
      }, {
        info: false,
        type: 'payment',
        'state': {
          'data': {
            'paymentSlip': {
              'paymentOrg': 'O=OrgA, L=São Paulo, C=BR',
              'code': '213234242445689783442432',
              'account': '4a543324-2737-4a03-b244-5d6f6c148d78',
              'createTime': '2019-02-27T20:16:18.292Z',
              'amount': 51.0
            },
            'linearId': {
              'externalId': null,
              'id': 'ea40aab5-d714-4a4e-b4c5-777f5e71f38a'
            },
            'participants': ['O=OrgA, L=São Paulo, C=BR']
          },
          'contract': 'tech.bluchain.democordanodes.contract.PaymentSlipContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '7280DBFA51F6E605D87414CA123D48F3CE44B24DAEC1291BC14DC1317E4F7BED'
          }
        },
        'ref': {
          'txhash': 'F4F7D968E15D02F38D6E5086AAF0FE6F52B1713FE58E184FA157D950CBE02E39',
          'index': 0
        }
      },
      {
        info: false,
        type: 'payment',
        'state': {
          'data': {
            'paymentSlip': {
              'paymentOrg': 'O=OrgA, L=São Paulo, C=BR',
              'code': '',
              'account': '4a543324-2737-4a03-b244-5d6f6c148d78',
              'createTime': '2019-02-27T20:16:18.292Z',
              'amount': 50.0
            },
            'linearId': {
              'externalId': null,
              'id': 'ea40aab5-d714-4a4e-b4c5-777f5e71f38a'
            },
            'participants': ['O=OrgA, L=São Paulo, C=BR']
          },
          'contract': 'tech.bluchain.democordanodes.contract.PaymentSlipContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '7280DBFA51F6E605D87414CA123D48F3CE44B24DAEC1291BC14DC1317E4F7BED'
          }
        },
        'ref': {
          'txhash': 'F4F7D968E15D02F38D6E5086AAF0FE6F52B1713FE58E184FA157D950CBE02E39',
          'index': 0
        }
      }
    ];
  }

}
