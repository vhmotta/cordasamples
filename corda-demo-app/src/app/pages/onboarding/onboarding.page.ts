import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';
import * as moment from 'moment';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AlertController, LoadingController } from '@ionic/angular';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';
import { PaguevelozService } from 'src/app/providers/pagueveloz/pagueveloz.service';
import { isCPF } from 'brazilian-values';
import { splitClasses } from '@angular/compiler';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.page.html',
  styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage implements OnInit {

  signupForm = new FormGroup({
    name: new FormControl('', Validators.compose([Validators.required])),
    // document: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9.-]{14}')]))
    document: new FormControl('', Validators.compose([Validators.required]))

  });

  org;
  orgName = window.location.hostname.split('.')[0];
  createLoading;

  constructor(
    public corda: CordaService,
    private router: Router,
    private alert: AlertsService,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private pagueVeloz: PaguevelozService) {

    /*
    this.corda.initNodeApp().then(() => {
      this.org = this.corda.getMyOrg();
      this.checkIfUser();
    });
    */

    this.checkIfUser();
    this.genLoading();

  }

  ngOnInit() {
    console.log('debugmode enabled');

    this.org = this.corda.orgData;
    this.corda.checkGovAccount()
    .then((resp: any) => {
      environment.governmentLinearId = resp.state.data.linearId.id;
      console.log(resp.state.data.linearId.id);
    })
    .catch( () => this.corda.createGovAccount()
      .then(() => console.log('Government Account Created')));
  }

  // valitadeCpf(value) {
  //   return isCPF(value);
  // }

  valitadeCpf(value) {
    return true;
  }

  recoveryAccount(document) {
    return Promise.resolve(
      this.corda.getAllUsers().then((resp: Array<any>) => {
        let user: any;
        try {
          resp.forEach(el => {
            if (el.state.data.account.document === document) {
              user = {
                orgName: el.state.data.account.orgName,
                name: el.state.data.account.name,
                linearId: el.state.data.linearId.id,
                document: document
              };

              localStorage.setItem('cordaUser.', JSON.stringify(user));
              throw user;
            }
          });
        } catch (e) {
          if (e !== user) { throw e; }
        }
        this.checkIfUser();
      })
    );
  }

  async genLoading() {
    this.createLoading =  await this.loadingCtrl.create({
        message: 'Entrando'
    });

  }

  errorAlert(error) {
    this.alert.errorAlert(error);
  }

  async createUser() {
    if (this.valitadeCpf(this.signupForm.value.document)) {
      this.createLoading.present();

      this.corda.createUserAccount(this.signupForm.value.name, 34.50, this.signupForm.value.document)
        .then(() => {
          // this.mockSlips(this.signupForm.value.name, this.signupForm.value.document, environment.tokens)
          // .then( resp => {
          //   console.log(resp);
            this.checkIfUser();
            this.createLoading.dismiss();
          // });
        })
        .catch(error => {
          console.log(error);
          if (error === 'Conta existente') {
            this.recoveryAccount(this.signupForm.value.document)
            .then( () => {
              this.createLoading.dismiss();
            });
          } else {
            this.createLoading.dismiss();
            this.errorAlert(error);
            console.log(error);
          }
        });

    } else {
      this.errorAlert('CPF Inválido');
    }

  }

  checkIfUser() {
    if (this.corda.hasLocalUser()) {
      console.log(this.getUser().orgName);
      console.log(this.corda.orgData.org);
      if (this.getUser().orgName === this.corda.orgData.org) {
        this.corda.getbalance();
        this.router.navigate(['/wallet']);
      } else {
        console.log('Usuário não é deste nó');
      }
    }
  }

  mockSlips(userName, document, tokens) {
    return new Promise((resolve) => {

  const promises = [];

      const slip = {
        'sacado': userName,
        'docSacado': document,
        'vencimento': '2019-04-25',
        'valor': '',
        'seuNumero': '',
        'parcela': '1 de 1',
        'linha1': 'Minha linha de teste 1',
        'linha2': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat maximus risus.',
        'email': 'luizsgpetri@gmail.com',
        'dataEnvioEmail': '05/03/2019',
        'pdf': true,
        'desconto': {
          'tipo': 'Isento',
          'valor': 0,
          'dataLimite': '2019-04-25'
        }
      };

      let promise;
      let value;
      let seuNumero;

      tokens.forEach(el => {

        value = Math.floor((Math.random() * 300) + 100);
        seuNumero = Math.floor((Math.random() * 99999) + 10000);
        slip.seuNumero = seuNumero.toString();
        slip.valor = value.toString() + '.00';

        promise = new Promise((resolve, reject) => {
          this.pagueVeloz.createBoleto(slip, el.token)
          .then(() => {
            console.log('boleto criado ' + el.name);
            return resolve('boleto criado ' + el.name);

          })
          .catch(() => {
            console.log('boleto não criado ' + el.name);
            return reject('boleto não criado ' + el.name);
          });
        });

        promises.push(promise);

      });

      Promise.all(promises).then(values => {
        return resolve(values);
      });

    });

  }


  getUser() {
    return JSON.parse(localStorage.getItem('cordaUser.'));
  }


}
