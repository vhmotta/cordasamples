import { Component, OnInit } from '@angular/core';
import { CordaService } from 'src/app/providers/corda/corda.service';
import * as moment from 'moment';
import { ModalController } from '@ionic/angular';
import { PayChargePage } from 'src/app/pages/modals/pay-charge/pay-charge.page';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PaguevelozService } from 'src/app/providers/pagueveloz/pagueveloz.service';
import { PaymentPage } from 'src/app/payment/payment.page';

@Component({
  selector: 'app-see-charges',
  templateUrl: './see-charges.page.html',
  styleUrls: ['./see-charges.page.scss'],
})
export class SeeChargesPage implements OnInit {
  chargesList = [];
  payablesList = [];
  user;
  moment = moment;
  orgs = environment.orgList;

  constructor(
    public corda: CordaService,
    private modalCtrl: ModalController,
    private router: Router,
    private pagueVeloz: PaguevelozService,
  ) {
   // this.mockData();
  }


  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('cordaUser.'));
    this.getCharges();
    this.getPayables();
  }

  toggleInfo(charge) {
    charge.info = !charge.info;
  }

  async openChargeModal(charge) {

    if (charge.state) {
      // this.router.navigate(['/charges/pay']);

      console.log('open modal charge clicked');
      const modal = await this.modalCtrl.create({
        component: PayChargePage,
        componentProps: {
          data: charge
        }
      });
      modal.present();
      const { data } = await modal.onDidDismiss();
      console.log(data);
    } else if (charge.codigoDeBarras) {
      console.log('open modal slip clicked');
      const modal = await this.modalCtrl.create({
        component: PaymentPage,
        componentProps: {
          slip: charge
        }
      });
      modal.present();
      const { data } = await modal.onDidDismiss();
      console.log(data);
    }
  }

  getCharges() {
    this.corda.directPaymentToPay(this.user.linearId)
    .then( (resp: Array<any>) => {
      this.chargesList = resp;
    })
    .catch(error => {
      // TODO do someting on error
    });
  }

  getPayables() {
    const payables = [];
    this.corda.directPaymentToPay(this.user.linearId)
      .then((resp: Array<any>) => {
        this.payablesList = payables.concat(resp);

        environment.tokens.forEach(el => {
          this.pagueVeloz.getMyBoletoDocument(this.user.document, el.token)
            .then((slips: Array<any>) => {
              slips.forEach(subEle => {
                const slip = Object.assign(subEle, {
                  issuer: el.name,
                  // timestamp: subEle.slipData.Emissao // TODO: buscar timestamp como data de emissao do boleto
                });
                this.payablesList.push(subEle);
                console.log(this.payablesList);
              });
            })
            .catch(error => {
              // TODO do someting on error
            });
        });

      })
      .catch(error => {
        // TODO do someting on error
      });
  }

  mockData() {
    this.chargesList = [
      {
        info: false,
        'state': {
          'data': {
            'directPayment': {
              'receiverOrg': 'O=OrgA, L=São Paulo, C=BR',
              'receiverAccount': '16c046a6-e3d9-462d-bd26-409651c127ba',
              'receiverName': 'Vitor',
              'receiverDocument': '123.123.123-12',
              'payerOrg': 'O=OrgA, L=São Paulo, C=BR',
              'payerAccount': 'a328d352-80dc-4fa9-80e5-a5fb8ffe86fd',
              'payerName': 'Joao',
              'payerDocument': '123.123.123-12',
              'createTime': '2019-02-28T14:40:05.628Z',
              'amount': 100.15,
              'status': 'PAID_OUT'
            },
            'linearId': {
              'externalId': null,
              'id': '0144b057-2bc8-487d-b141-bdf48670a523'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgA, L=São Paulo, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.DirectPaymentContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': 'AF9BE6D9C463B2CC7A81E9A9BA8FCDF92233D5F0BCBA7F7579BCCF47FB367552'
          }
        },
        'ref': {
          'txhash': 'AF906A62278128BBD2740A26CA643508B1424E059EBBAF08C31624FA15B0ECEA',
          'index': 0
        }
      },
      {
        info: false,
        'state': {
          'data': {
            'directPayment': {
              'receiverOrg': 'O=OrgB, L=São Paulo, C=BR',
              'receiverAccount': '16c046a6-e3d9-462d-bd26-409651c127ba',
              'receiverName': 'Vitor',
              'receiverDocument': '123.123.123-12',
              'payerOrg': 'O=OrgA, L=São Paulo, C=BR',
              'payerAccount': 'a328d352-80dc-4fa9-80e5-a5fb8ffe86fd',
              'payerName': 'Joao',
              'payerDocument': '123.123.123-12',
              'createTime': '2019-02-28T17:02:02.145Z',
              'amount': 100,
              'status': 'PENDING'
            },
            'linearId': {
              'externalId': null,
              'id': 'b38e31e4-be76-4dfe-a608-ea4c7869be7b'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgA, L=São Paulo, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.DirectPaymentContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': 'AF9BE6D9C463B2CC7A81E9A9BA8FCDF92233D5F0BCBA7F7579BCCF47FB367552'
          }
        },
        'ref': {
          'txhash': 'F9790FFB4D6770E600A8C7CAC494CA65C12ACBF616E82E27E43461054C285505',
          'index': 0
        }
      },
    ];
  }
}
