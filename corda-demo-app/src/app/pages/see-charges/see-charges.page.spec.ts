import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeChargesPage } from './see-charges.page';

describe('SeeChargesPage', () => {
  let component: SeeChargesPage;
  let fixture: ComponentFixture<SeeChargesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeChargesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeChargesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
