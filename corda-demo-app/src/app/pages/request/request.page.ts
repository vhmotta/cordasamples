import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';
import { SocialService } from 'src/app/providers/social/social.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
// tslint:disable-next-line:component-class-suffix
export class RequestPage implements OnInit {
  user;
  qrValue;
  encodedUrl;
  request;

  requestForm = new FormGroup({
    amount: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+[,][0-9]{1,3}')])),
  });

  constructor(
    public corda: CordaService,
    private router: Router,
    private alert: AlertsService,
    private social: SocialService
    ) {
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('cordaUser.'));
  }

  ionViewDidEnter() {
  }

  sendRequest() {
    this.request = {
      accountTo: this.user.linearId,
      accountToName: this.user.name,
      amount: this.requestForm.value.amount,
      orgTo: this.user.orgName,
      orgToName: this.corda.orgData.name
    };

    this.qrValue = JSON.stringify(this.request);

  }

  shareOnWhatsApp() {
    this.social.whatsappShareRequest(this.user, this.requestForm.value.amount, 'transfer')
    .then(waUrl => {
      this.social.shotnerUrl(waUrl)
      .then(shorty => {
        const message = this.user.name + ' enviou uma cobrança para você. Acesse em: '; // + shorty;
        this.social.whatsappShare(message)
        .then((whatsAppLink: string) => {
          console.log(whatsAppLink);
          window.location.href = whatsAppLink;
        });
      })
      .catch(error => {
        this.alert.errorAlert(error);
      });
    });



  }

  clearQr() {
    this.requestForm.reset();
    this.qrValue = null;
    this.router.navigate(['/wallet']);
  }


}
