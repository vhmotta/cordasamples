import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController, LoadingController } from '@ionic/angular';
import { QrReaderPage } from 'src/app/pages/modals/qr-reader/qr-reader.page';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.page.html',
  styleUrls: ['./transfer.page.scss'],
})
export class TransferPage implements OnInit {

  hasReceivedData = false;
  isTransfer = false;
  isCharge = false;
  transferLoading;
  user;
  orgs = environment.orgList;
  orgTo;

  transferForm = new FormGroup({
    accountToName: new FormControl('', Validators.compose([Validators.required])),
    accountTo: new FormControl('', Validators.compose([Validators.required])),
    orgTo: new FormControl('', Validators.compose([Validators.required])),
    orgToName: new FormControl('', Validators.compose([Validators.required])),
    amount: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('[0-9]+[,][0-9]{1,3}'),
      // TODO: validar amount do token selecionado, e não do total de tokens
    ])),
  });

  constructor(
      private modalCtrl: ModalController,
      public corda: CordaService,
      public router: Router,
      private loadingCtrl: LoadingController,
      private route: ActivatedRoute,
      public alert: AlertsService,

        ) {  }

  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('cordaUser.'));

    this.genLoading();
    this.mockOrgList();

    this.route.queryParams.subscribe(params => {

      console.log(params);

      const urlTransfer = {
        accountTo: params['accountTo'],
        accountToName: params['accountToName'],
        amount: parseFloat(params['amount']).toFixed(2),
        orgTo: params['orgTo'],
        orgToName: params['orgToName'],
      };

      console.log(urlTransfer);

      if (params.type === 'charge') {
        this.isCharge = true;

        this.transferForm.patchValue(urlTransfer);
        this.orgTo = urlTransfer.orgTo;
        this.hasReceivedData = true;

      } else if (params.type === 'transfer') {
        this.isTransfer = true;

        console.log(params);
        console.log(urlTransfer); // Print the parameter to the console.

        this.transferForm.patchValue(urlTransfer);
        this.orgTo = urlTransfer.orgTo;

        this.hasReceivedData = true;

      } else {

        this.openQrModal();

      }
  });

  }

  async openQrModal() {
    const modal = await this.modalCtrl.create({
      component: QrReaderPage
    });
    modal.present();
    const { data } = await modal.onDidDismiss();
    console.log(data);
    // TODO: validar se dados do qr code são válidos
    if (data && data !== 'close') {
      this.transferForm.patchValue(data);
      this.hasReceivedData = true;
    }
  }

  pay() {
    if (this.isCharge) {
      this.payCharge();
    } else {
      this.sendTransfer();
    }
  }

  sendTransfer() {

    this.transferLoading.present();
    const user = JSON.parse(localStorage.getItem('cordaUser.'));
    const transfer = {
      accountTo: this.transferForm.value.accountTo,
      accountToName: this.transferForm.value.accountToName,
      orgTo: this.transferForm.value.orgTo,
      accountFrom: user.linearId,
      accountFromName: user.name,
      amount: this.parseAmountTx(this.transferForm.value.amount),
    };

    this.corda.makeTransfer(transfer.accountFrom, transfer.orgTo, transfer.accountTo, transfer.accountToName, transfer.amount).then(res => {
      console.log(res);
      this.transferLoading.dismiss();
      this.router.navigate(['/wallet']);
    })
        .catch(error => {
          this.transferLoading.dismiss();
          this.presentAlertConfirm(error);
        });

  }


  payCharge() {
    this.corda.directPaymentPay(this.user.linearId, this.transferForm.value.accountTo)
    .then(success => {
      this.alert.successAlertModal('Pagamento Realizado', 'Pagamento da cobrança direta foi realizado com sucesso', 'wallet');
    })
    .catch(error => {
      this.alert.errorAlert(error);
    });
  }


  async presentAlertConfirm(error) {
    this.alert.errorAlert(error);
    this.router.navigate(['/wallet']);
  }


  parseAmountTx(amount): number {
    const parsedAmount = parseFloat(amount.replace(',', '.'));
    return parsedAmount;
  }

  async genLoading() {
    this.transferLoading = await this.loadingCtrl.create({
      message: 'Realizando transferência'
    });
  }

    mockOrgList() {
    this.orgs = Object.assign(this.orgs, {
      'O=OrgX, L=São Paulo, C=BR': 'Banco do Brasil',
      'O=OrgY, L=São Paulo, C=BR': 'Santander',
      'O=OrgZ, L=São Paulo, C=BR': 'Sicred',
    });
  }

}
