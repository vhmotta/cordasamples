import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { ModalController } from '@ionic/angular';
import { PayChargePage } from 'src/app/pages/modals/pay-charge/pay-charge.page';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';
import { SocialService } from 'src/app/providers/social/social.service';
import { Router } from '@angular/router';
import { PaguevelozService } from 'src/app/providers/pagueveloz/pagueveloz.service';
import { PaymentPage } from 'src/app/payment/payment.page';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {

  history = [
    {
      timestamp: '29 Abr 19',
      amount: -15.00
    },
    {
      timestamp: '27 Abr 19',
      amount: -5.50
    },
    {
      timestamp: '27 Abr 19',
      amount: 100.00
    },
  ];


  isDisplayBox = false;

  // chargesList;
  theme = this.corda.orgData.theme;
  moment = moment;
  user;
  logo = 'cobranca';

  // MOCK data
  transferList = [];
  chargesList = [];
  chargesListReceive = [];
  slipsList = [];
  payablesList = [];

  constructor(
    public corda: CordaService,
    private alert: AlertsService,
    private modalCtrl: ModalController,
    private social: SocialService,
    private router: Router,
    private pagueVeloz: PaguevelozService,
  ) {
    this.user = this.getUser();
    // this.mockData();
  }

  ngOnInit() {

    // this.getMockHistory();
    this.getHistory();
    // this.getCharges();
    // this.getSlips(this.user.document, environment.tokens);
    // this.getReceiveCharges();
    // this.getPayables();
    // this.getMockBoletos();

    setInterval(() => {
      // this.getMockHistory();
      this.getHistory();
      // this.getCharges();
      // this.getSlips(this.user.document, environment.tokens);
      // this.getReceiveCharges();
    }, 3000);
  }

  ionViewDidEnter() {
    // this.getPayables();
  }


  showUserInfo() {
    const user = this.corda.userData;
    // tslint:disable-next-line:max-line-length
    const info = `
    <p>Nome: ${user.account.name}</p>
    <br>
    <p>Tel: ${user.account.document}</p>
    <br>
    `;
    const infoHeader = `Informações do Usuário`;
    this.alert.infoAlert(info, infoHeader);
  }

  getUser() {
    return JSON.parse(localStorage.getItem('cordaUser.'));
  }

  getHistory() {
    this.corda.getHistory(this.user.linearId).then((history: Array<any>) => {
      this.transferList = history.slice(0, 2);
    });
  }

  async openChargeModal(charge) {

    if (charge.state) {
      // this.router.navigate(['/charges/pay']);

      console.log('open modal charge clicked');
      const modal = await this.modalCtrl.create({
        component: PayChargePage,
        componentProps: {
          data: charge
        }
      });
      modal.present();
      const { data } = await modal.onDidDismiss();
      console.log(data);
    } else if (charge.codigoDeBarras) {
      console.log('open modal slip clicked');
      const modal = await this.modalCtrl.create({
        component: PaymentPage,
        componentProps: {
          slip: charge
        }
      });
      modal.present();
      const { data } = await modal.onDidDismiss();
      console.log(data);
    }
  }

  getPayables() {
    const payables = [];
    this.corda.directPaymentToPay(this.user.linearId)
      .then((resp: Array<any>) => {
        this.payablesList = payables.concat(resp);

        environment.tokens.forEach(el => {
          this.pagueVeloz.getMyBoletoDocument(this.user.document, el.token)
            .then((slips: Array<any>) => {
              slips.forEach(subEle => {
                const slip = Object.assign(subEle, {
                  issuer: el.name,
                  // timestamp: subEle.slipData.Emissao // TODO: buscar timestamp como data de emissao do boleto
                });
                this.payablesList.push(subEle);
                console.log(this.payablesList);
              });
            })
            .catch(error => {
              // TODO do someting on error
            });
        });

      })
      .catch(error => {
        // TODO do someting on error
      });
  }

  getCharges() {
    this.corda.directPaymentToPay(this.user.linearId)
      .then((resp: Array<any>) => {
        this.chargesList = resp.slice(0, 2);
      })
      .catch(error => {
        // TODO do someting on error
      });
  }

  getSlips(document, tokens) {

    const list = [];

    tokens.forEach(el => {
      this.pagueVeloz.getMyBoletoDocument(document, el.token)
        .then((resp: Array<any>) => {
          resp.forEach(subEle => {
            const slip = Object.assign(subEle, {
              issuer: el.name,
            });
            list.push(subEle);
            console.log(list);
          });
        })
        .catch(error => {
          // TODO do someting on error
        });
    });
    this.slipsList = list;
  }


  getReceiveCharges() {
    this.corda.directPaymentToReceive(this.user.linearId)
      .then((resp: Array<any>) => {
        this.chargesListReceive = resp.slice(0, 2);
      })
      .catch(error => {
        // TODO do someting on error
      });
  }

  shareOnwhatsApp(rqLinearId, amount) {
    this.social.whatsappShareCharge(this.user, rqLinearId, amount, 'charge')
      .then(waUrl => {
        console.log(waUrl);
        this.social.shotnerUrl(waUrl)
          .then(shorty => {
            console.log(shorty);
            // tslint:disable-next-line:max-line-length
            const message = this.user.name + '%20enviou%20uma%20cobran%C3%A7a%20para%20voc%C3%AA%2C%20confira%20no%20aplicativo. '; // + shorty;
            this.social.whatsappShare(message)
              .then((url: string) => {
                console.log(url);
                this.router.navigate(['/wallet']);
                window.location.href = url;
              })
              .catch(error => {
                this.alert.errorAlert(error);
              })
              .catch(error => {
                this.alert.errorAlert(error);
              });
          });
      });
  }

  openBox() {
    this.isDisplayBox = true;
  }

  closeBox() {
    this.isDisplayBox = false;
  }


  getMockBoletos() {
    this.payablesList = [
      {
        issuer: 'pague veloz',
        'vencimento': '25/04/2019',
        'vencido': false,
        'linha1': 'Minha linha de teste 1',
        'linha2': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat maximus risus.',
        'docSacado': '644.382.430-09',
        'sacado': 'Teste',
        'valor': 129,
        'url': 'https://sandbox.pagueveloz.com.br/Boleto/01696787000000129000002001007014900000153893',
        'linhaDigitavel': '01690.00205 01007.014903 00001.538933 6 78700000012900',
        'status': 4,
        'pago': false,
        'userId': 'LToEDnCA0FPvnwKHBoA6',
        'codigoDeBarras': '01696787000000129000002001007014900000153893',
        'id': 22367,
        'nossoNumero': '1/001/0000015389-3',
        'seuNumero': '12009',
        'cancelado': false,
        'dataPagamento': null
      },
      {
        issuer: 'eletropaulo',
        'pago': false,
        'userId': 'LToEDnCA0FPvnwKHBoA6',
        'codigoDeBarras': '01691787000000118000002001007014900000153745',
        'id': 22353,
        'nossoNumero': '1/001/0000015374-5',
        'seuNumero': '58307',
        'cancelado': false,
        'dataPagamento': null,
        'vencimento': '25/04/2019',
        'vencido': false,
        'linha1': 'Minha linha de teste 1',
        'linha2': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat maximus risus.',
        'docSacado': '258.569.540-92',
        'sacado': 'thiago',
        'valor': 118,
        'url': 'https://sandbox.pagueveloz.com.br/Boleto/01691787000000118000002001007014900000153745',
        'linhaDigitavel': '01690.00205 01007.014903 00001.537455 1 78700000011800',
        'status': 4
      },
    ];
  }


  getMockHistory() {
    this.transferList = [
      {
        info: false,
        'state':
        {
          'data':
          {
            'transfer':
            {
              'orgFrom': 'O=OrgA, L=São Paulo, C=BR',
              'accountFrom': '8a7e46f6-8ca5-452e-9c3c-03662849a695',
              'accountFromName': 'Juvenal',
              'orgTo': 'O=OrgB, L=Campinas, C=BR',
              'accountTo': '4aa359cb-6b07-4f83-bec6-b0d4e6b11fd1',
              'accountToName': 'Joao',
              'createTime': '2019-02-21T12:38:31.413Z',
              'amount': 100,
              'status': 'SHARED'
            },
            'linearId': {
              'externalId': null,
              'id': '8ea9081b-cfed-4c49-974a-14711c15c4a3'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgB, L=Campinas, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.TransferContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '98277E6CCA1334E820D9D91B35E33BB6CFA45D86F811E8A0113873E89E4C845F'
          }
        },
        'ref': {
          'txhash': 'D64797B97AAFEB5515B0E617AAB29D82EFE270A4AF801D64292BCDABE169348D',
          'index': 1
        }
      },
      {
        info: false,
        'state':
        {
          'data':
          {
            'transfer':
            {
              'orgFrom': 'O=OrgA, L=São Paulo, C=BR',
              'accountFrom': '4aa359cb-6b07-4f83-bec6-b0d4e6b11fd1',
              'accountFromName': 'joao',
              'orgTo': 'O=OrgB, L=Campinas, C=BR',
              'accountTo': 'd4ae05d7-9f5f-4d5f-b54d-31a9d3750d30',
              'accountToName': 'Juvenal',
              'createTime': '2019-02-21T12:38:31.413Z',
              'amount': 100,
              'status': 'SHARED'
            },
            'linearId': {
              'externalId': null,
              'id': '8ea9081b-cfed-4c49-974a-14711c15c4a3'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgB, L=Campinas, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.TransferContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '98277E6CCA1334E820D9D91B35E33BB6CFA45D86F811E8A0113873E89E4C845F'
          }
        },
        'ref': {
          'txhash': 'D64797B97AAFEB5515B0E617AAB29D82EFE270A4AF801D64292BCDABE169348D',
          'index': 1
        }
      }
    ];
  }

  mockData() {
    this.transferList = [
      {
        info: false,
        'state': {
          'data': {
            'directPayment': {
              'receiverOrg': 'O=OrgA, L=São Paulo, C=BR',
              'receiverAccount': '16c046a6-e3d9-462d-bd26-409651c127ba',
              'receiverName': 'Vitor',
              'receiverDocument': '123.123.123-12',
              'payerOrg': 'O=OrgA, L=São Paulo, C=BR',
              'payerAccount': 'a328d352-80dc-4fa9-80e5-a5fb8ffe86fd',
              'payerName': 'Joao',
              'payerDocument': '123.123.123-12',
              'createTime': '2019-02-28T14:40:05.628Z',
              'amount': 100.15,
              'status': 'PAID_OUT'
            },
            'linearId': {
              'externalId': null,
              'id': '0144b057-2bc8-487d-b141-bdf48670a523'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgA, L=São Paulo, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.DirectPaymentContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': 'AF9BE6D9C463B2CC7A81E9A9BA8FCDF92233D5F0BCBA7F7579BCCF47FB367552'
          }
        },
        'ref': {
          'txhash': 'AF906A62278128BBD2740A26CA643508B1424E059EBBAF08C31624FA15B0ECEA',
          'index': 0
        }
      },
      {
        info: false,
        'state': {
          'data': {
            'directPayment': {
              'receiverOrg': 'O=OrgB, L=São Paulo, C=BR',
              'receiverAccount': '16c046a6-e3d9-462d-bd26-409651c127ba',
              'receiverName': 'Vitor',
              'receiverDocument': '123.123.123-12',
              'payerOrg': 'O=OrgA, L=São Paulo, C=BR',
              'payerAccount': 'a328d352-80dc-4fa9-80e5-a5fb8ffe86fd',
              'payerName': 'Joao',
              'payerDocument': '123.123.123-12',
              'createTime': '2019-02-28T17:02:02.145Z',
              'amount': 100,
              'status': 'PENDING'
            },
            'linearId': {
              'externalId': null,
              'id': 'b38e31e4-be76-4dfe-a608-ea4c7869be7b'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgA, L=São Paulo, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.DirectPaymentContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': 'AF9BE6D9C463B2CC7A81E9A9BA8FCDF92233D5F0BCBA7F7579BCCF47FB367552'
          }
        },
        'ref': {
          'txhash': 'F9790FFB4D6770E600A8C7CAC494CA65C12ACBF616E82E27E43461054C285505',
          'index': 0
        }
      },
      {
        info: false,
        type: 'payment',
        'state': {
          'data': {
            'paymentSlip': {
              'paymentOrg': 'O=OrgA, L=São Paulo, C=BR',
              'code': '213234242445689783442432',
              'account': '4a543324-2737-4a03-b244-5d6f6c148d78',
              'createTime': '2019-02-27T20:16:18.292Z',
              'amount': 51.0
            },
            'linearId': {
              'externalId': null,
              'id': 'ea40aab5-d714-4a4e-b4c5-777f5e71f38a'
            },
            'participants': ['O=OrgA, L=São Paulo, C=BR']
          },
          'contract': 'tech.bluchain.democordanodes.contract.PaymentSlipContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '7280DBFA51F6E605D87414CA123D48F3CE44B24DAEC1291BC14DC1317E4F7BED'
          }
        },
        'ref': {
          'txhash': 'F4F7D968E15D02F38D6E5086AAF0FE6F52B1713FE58E184FA157D950CBE02E39',
          'index': 0
        }
      },
      {
        info: false,
        type: 'payment',
        'state': {
          'data': {
            'paymentSlip': {
              'paymentOrg': 'O=OrgA, L=São Paulo, C=BR',
              'code': '',
              'account': '4a543324-2737-4a03-b244-5d6f6c148d78',
              'createTime': '2019-02-27T20:16:18.292Z',
              'amount': 50.0
            },
            'linearId': {
              'externalId': null,
              'id': 'ea40aab5-d714-4a4e-b4c5-777f5e71f38a'
            },
            'participants': ['O=OrgA, L=São Paulo, C=BR']
          },
          'contract': 'tech.bluchain.democordanodes.contract.PaymentSlipContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': '7280DBFA51F6E605D87414CA123D48F3CE44B24DAEC1291BC14DC1317E4F7BED'
          }
        },
        'ref': {
          'txhash': 'F4F7D968E15D02F38D6E5086AAF0FE6F52B1713FE58E184FA157D950CBE02E39',
          'index': 0
        }
      }
    ];
  }
}
