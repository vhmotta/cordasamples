import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayChargePage } from './pay-charge.page';

describe('PayChargePage', () => {
  let component: PayChargePage;
  let fixture: ComponentFixture<PayChargePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayChargePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayChargePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
