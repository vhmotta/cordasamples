import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { ModalController, NavParams } from '@ionic/angular';
import { CreateChargePage } from '../../create-charge/create-charge.page';
import { environment } from 'src/environments/environment';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';


@Component({
  selector: 'app-pay-charge',
  templateUrl: './pay-charge.page.html',
  styleUrls: ['./pay-charge.page.scss'],
})
export class PayChargePage implements OnInit {
  user;
  charge = this.navParams.get('data');
  orgs = environment.orgList;

  chargeForm = new FormGroup({
    receiverName: new FormControl('', Validators.compose([Validators.required])),
    receiverDocument: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9.-]{14}|[0-9./-]{18}')])),
    receiverAccount: new FormControl('', Validators.compose([Validators.required])),
    receiverOrg: new FormControl('', Validators.compose([Validators.required])),
    receiverOrgName: new FormControl('', Validators.compose([Validators.required])),
    amount: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('[0-9]+[,][0-9]{1,3}'),
    ])),
  });

  hasReceivedData = true;

  constructor(
    public corda: CordaService,
    private modalCtrl: ModalController,
    private navParams: NavParams,
    public alert: AlertsService,
    ) {

    }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('cordaUser.'));

    this.chargeForm.patchValue({
      receiverName: this.charge.state.data.directPayment.receiverName,
      receiverDocument: this.charge.state.data.directPayment.receiverDocument,
      receiverAccount: this.charge.state.data.directPayment.receiverAccount,
      receiverOrg: this.charge.state.data.directPayment.receiverOrg,
      receiverOrgName: this.orgs[this.charge.state.data.directPayment.receiverOrg],
      amount: this.charge.state.data.directPayment.amount.toFixed(2)
    });
  }

  parseAmountTx(amount): number {
    const parsedAmount = parseFloat(amount.replace(',', '.'));
    return parsedAmount;
  }

  dismissModal() {
    this.modalCtrl.dismiss();
  }

  payCharge() {
    this.corda.directPaymentPay(this.user.linearId, this.charge.state.data.linearId.id)
    .then(success => {
      this.alert.successAlertModal('Pagamento Realizado', 'Pagamento da cobrança direta foi realizado com sucesso', 'wallet');
    })
    .catch(error => {
      this.alert.errorAlert(error);
    });
  }

  async successAlert() {
    }

}
