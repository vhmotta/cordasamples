import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
// TODO: importar interfaces de tipos de payload de qrcodes
import { TransactionRequest } from 'src/app/models/qrcode';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qr-reader',
  templateUrl: './qr-reader.page.html',
  styleUrls: ['./qr-reader.page.scss'],
})
export class QrReaderPage implements OnInit {

  @Input() hasBarcode;

  constructor(
    private modalCtrl: ModalController,
    private router: Router
    ) { }

  @ViewChild('scanner')
  scanner: ZXingScannerComponent;
  videoElement: any;
  hasDevices: boolean;
  hasPermission: boolean;

  availableDevices: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo;

  ngOnInit() {
    this.scanner.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
      this.hasDevices = true;
      this.availableDevices = devices;
      console.log(devices);
      const camera = devices[0].deviceId;
      this.onDeviceSelectChange(camera);
      // selects the devices's back camera by default
      for (const device of devices) {
        if (/back|rear|environment/gi.test(device.label)) {
          this.scanner.changeDevice(device);
          this.currentDevice = device;
          break;
        }
      }
    });

    this.scanner.camerasNotFound.subscribe(() => this.hasDevices = false);
    this.scanner.permissionResponse.subscribe((perm: boolean) => this.hasPermission = perm);
  }

  async scanCompleteHandler(event) {
    console.log(event.text);
    this.identifyQr(event).then(typeQr => {
      this.handleQr({type: typeQr, payload: event.text});
    });
    // console.log(event);
  }

  onDeviceSelectChange(selectedValue: string) {
    console.log('Selection changed: ', selectedValue);
    this.currentDevice = this.scanner.getDeviceById(selectedValue);
  }


  // mock data
  dismissModal() {
    this.modalCtrl.dismiss('close');
    this.router.navigate(['/wallet']);
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  // TODO: adicionar outros tipos de payloads conforme necessário
  async identifyQr(event) {

    let response = 'unknown';
    try {
      // console.log(JSON.parse(event.text));
      if (this.isPaymentSlip(event.text)) {
        console.log('passed isPaymentSlip');
        response = 'payment';

      } else if (this.isTransaction(JSON.parse(event.text))) {
        console.log('passed isTransaction');
        response = 'transaction';
      } else {
        response = 'unknown';
      }
    } finally {
      return response;
    }

  }

  async handleQr(arg: {type: string, payload: string}) {
    switch (arg.type) {
      case 'transaction': {
        const parsedPayload = JSON.parse(arg.payload);
        this.modalCtrl.dismiss(<TransactionRequest>parsedPayload);
        break;
      }
      case 'payment': {
        const parsedPayload = arg.payload;
        this.modalCtrl.dismiss(parsedPayload);
        break;
      }
      default: {
        console.log('not a supported format');
        this.modalCtrl.dismiss();
        break;
      }
    }
  }

  isTransaction(object: any): object is TransactionRequest {
    return (<TransactionRequest>object).amount !== undefined;
  }

  isPaymentSlip(num) {
    console.log(num);
    if (!isNaN(num)) {
      return true;
    } else {
      return false;
    }
  }

}
