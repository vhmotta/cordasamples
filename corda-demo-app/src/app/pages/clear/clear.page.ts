import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-clear',
  templateUrl: './clear.page.html',
  styleUrls: ['./clear.page.scss'],
})
export class ClearPage implements OnInit {

  constructor(
      public router: Router,
  ) { }

  ngOnInit() {
    localStorage.removeItem('cordaUser.');
    this.router.navigate(['/']);

  }

}
