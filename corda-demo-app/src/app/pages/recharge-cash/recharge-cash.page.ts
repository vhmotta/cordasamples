import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recharge-cash',
  templateUrl: './recharge-cash.page.html',
  styleUrls: ['./recharge-cash.page.scss'],
})
export class RechargeCashPage implements OnInit {

  rechargePoints: Array<any>;

  constructor() { }

  ngOnInit() {
    this.mockPoints();
  }

  mockPoints() {
    this.rechargePoints = [
      {
        name: 'Financeiro CieloBlu',
        location: `R. Alfredo da Costa Figo, 736
        Fazenda Santa Cândida
        Campinas - SP
        1o. Andar`,
        hours: '8:00 às 18:00'
      },
    ];
  }

}
