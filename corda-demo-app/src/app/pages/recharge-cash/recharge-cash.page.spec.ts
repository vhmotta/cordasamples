import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RechargeCashPage } from './recharge-cash.page';

describe('RechargeCashPage', () => {
  let component: RechargeCashPage;
  let fixture: ComponentFixture<RechargeCashPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechargeCashPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RechargeCashPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
