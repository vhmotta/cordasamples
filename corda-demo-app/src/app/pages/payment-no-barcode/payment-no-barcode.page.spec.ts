import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentNoBarcodePage } from './payment-no-barcode.page';

describe('PaymentNoBarcodePage', () => {
  let component: PaymentNoBarcodePage;
  let fixture: ComponentFixture<PaymentNoBarcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentNoBarcodePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentNoBarcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
