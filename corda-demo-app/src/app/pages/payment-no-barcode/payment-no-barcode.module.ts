import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PaymentNoBarcodePage } from './payment-no-barcode.page';
import { ComponentsModule } from 'src/app/components/components.module';

import { BrMaskerModule } from 'br-mask';

const routes: Routes = [
  {
    path: '',
    component: PaymentNoBarcodePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    BrMaskerModule,
    ReactiveFormsModule
  ],
  declarations: [PaymentNoBarcodePage]
})
export class PaymentNoBarcodePageModule {}
