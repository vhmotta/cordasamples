import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { Router } from '@angular/router';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';
import { LoadingController } from '@ionic/angular';



@Component({
  selector: 'app-payment-no-barcode',
  templateUrl: './payment-no-barcode.page.html',
  styleUrls: ['./payment-no-barcode.page.scss'],
})
export class PaymentNoBarcodePage implements OnInit {

  step = 1;
  amount;

  paymentNoBarcode = new FormGroup({
    paymentType: new FormControl('', Validators.compose([Validators.required])),
    name: new FormControl('', Validators.compose([Validators.required])),
    phone: new FormControl('', Validators.compose([Validators.required])),
    calculationPeriod: new FormControl('', Validators.compose([Validators.required])),
    cpfCnpj: new FormControl('', Validators.compose([Validators.required])),
    recipeCode: new FormControl('', Validators.compose([Validators.required])),
    referenceNumber: new FormControl(''),
    dueDate: new FormControl('', Validators.compose([Validators.required])),
    mainValue: new FormControl('', Validators.compose([Validators.required])),
    fineAmount: new FormControl(''),
    interestAmount: new FormControl(''),
    amount: new FormControl('', Validators.compose([Validators.required])),
  });

  payLoading;



  constructor(
    public corda: CordaService,
    private router: Router,
    public alert: AlertsService,
    private loadingCtrl: LoadingController,
    ) {

    }

  ngOnInit() {
    this.genLoading();
  }

  async genLoading() {
    this.payLoading =  await this.loadingCtrl.create({
        message: 'Realizando transferência'
    });

  }

  getUser() {
    return JSON.parse(localStorage.getItem('cordaUser.'));
  }

  pay() {
    this.payDarf(parseFloat(this.amount.replace(',', '.')));
  }

  payDarf(amount) {
    this.payLoading.present();
    const from = this.getUser().linearId;
    this.corda.payGovernmentTax(from, amount)
    .then(success => {
      this.alert.successAlertModal('Pagamento Realizado', 'Pagamento da DARF foi realizado com sucesso', 'wallet');
      this.payLoading.dismiss();
    })
    .catch(error => {
      this.alert.errorAlert(error);
      this.payLoading.dismiss();
    });
  }

  setStep(step) {
    this.step = step;
  }

  changeValue() {
    const controls = this.paymentNoBarcode.controls;
    let values = [
      controls.mainValue.value || '0',
      controls.fineAmount.value || '0',
      controls.interestAmount.value || '0'
    ];
    values = values.map(element => parseFloat(element.replace(',', '.')));
    this.amount = values.reduce((accumulated, item) => accumulated + item).toFixed(2).replace('.', ',');
  }

}
