import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateChargePage } from './create-charge.page';

describe('CreateChargePage', () => {
  let component: CreateChargePage;
  let fixture: ComponentFixture<CreateChargePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateChargePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateChargePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
