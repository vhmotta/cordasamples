import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { Router } from '@angular/router';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';
import { SocialService } from 'src/app/providers/social/social.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-charge',
  templateUrl: './create-charge.page.html',
  styleUrls: ['./create-charge.page.scss'],
})
export class CreateChargePage implements OnInit {
  user;
  createLoading;
  charge;
  step = 1;
  orgs = environment.orgList;

  createChargeForm = new FormGroup({
    amount: new FormControl('', Validators.compose([Validators.required])),
    payerOrg: new FormControl('', Validators.compose([Validators.required])),
    payerDocument: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9.-]{14}|[0-9./-]{18}')])),
  });

  constructor(
    public corda: CordaService,
    public alert: AlertsService,
    private router: Router,
    private social: SocialService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('cordaUser.'));
    this.mockOrgList();
  }


  createCharge() {

    const user = JSON.parse(localStorage.getItem('cordaUser.'));

    const charge = {
      receiverAccount: user.linearId,
      payerOrg: this.createChargeForm.value.payerOrg,
      payerDocument: this.createChargeForm.value.payerDocument,
      amount: parseFloat(this.createChargeForm.value.amount.replace(',', '.'))
    };

    console.log(charge);

    this.corda.directPaymentCreate(charge)
    .then((resp: any) => {
      const receiver =  resp.data.receiverName;
      const chageData = resp.data.directPayment;
      const chargeId = resp.data.linearId.id;
      console.log(chageData);
      console.log(chargeId);
      this.alertOnSuccess(resp.data.linearId.id, charge.amount);
    })
    .catch(error => {
      this.alert.errorAlert(error);
    });

  }

  alertOnSuccess(rqLinearId, amount ) {
    this.social.whatsappShareCharge(this.user, rqLinearId, amount, 'charge')
    .then(waUrl => {
      this.social.shotnerUrl(waUrl)
      .then(shorty => {
        // tslint:disable-next-line:max-line-length
        const message = this.user.name + '%20enviou%20uma%20cobran%C3%A7a%20para%20voc%C3%AA%20confira%20no%20aplicativo.%20Acesse%20em:%20' + shorty;
       console.log(message);
        // tslint:disable-next-line:max-line-length
        this.alert.successWaAlert('Cobrança Criada', 'A cobrança foi criada com sucesso e já está disponível para o sacado', 'wallet', message);
      })
      .catch(error => {
        this.alert.errorAlert(error);
      });
    });
  }

  clear() {
    this.charge = undefined;
    this.createChargeForm.reset();
    this.router.navigate(['/wallet']);
  }

  setStep(step) {
    this.step = step;
  }

  mockOrgList() {
    this.orgs = Object.assign(this.orgs, {
      'O=OrgX, L=São Paulo, C=BR': 'Banco do Brasil',
      'O=OrgY, L=São Paulo, C=BR': 'Santander',
      'O=OrgZ, L=São Paulo, C=BR': 'Sicred',
    });
  }

}
