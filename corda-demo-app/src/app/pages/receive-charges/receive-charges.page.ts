import { Component, OnInit } from '@angular/core';
import { CordaService } from 'src/app/providers/corda/corda.service';
import * as moment from 'moment';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { SocialService } from 'src/app/providers/social/social.service';
import { AlertsService } from 'src/app/providers/alerts/alerts.service';

@Component({
  selector: 'app-receive-charges',
  templateUrl: './receive-charges.page.html',
  styleUrls: ['./receive-charges.page.scss'],
})
export class ReceiveChargesPage implements OnInit {
  chargesList = [];
  user;
  moment = moment;
  orgs = environment.orgList;

  constructor(
    public corda: CordaService,
    private modalCtrl: ModalController,
    private social: SocialService,
    private router: Router,
    private alert: AlertsService,
  ) {
  //  this.mockData();
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('cordaUser.'));
    this.getCharges();
  }

  getCharges() {
    this.corda.directPaymentToReceive(this.user.linearId)
    .then( (resp: Array<any>) => {
      console.log(resp);
      this.chargesList = resp;
    })
    .catch(error => {
      // TODO do someting on error
    });
  }

  shareOnwhatsApp(rqLinearId, amount ) {
    this.social.whatsappShareCharge(this.user, rqLinearId, amount, 'charge')
    .then(waUrl => {
      console.log(waUrl);
      this.social.shotnerUrl(waUrl)
      .then(shorty => {
        console.log(shorty);
       // tslint:disable-next-line:max-line-length
       const message = this.user.name + '%20enviou%20uma%20cobran%C3%A7a%20para%20voc%C3%AA%2C%20confira%20no%20aplicativo. ' ; //+ shorty;
       this.social.whatsappShare(message)
        .then((url: string) => {
          console.log(url);
          this.router.navigate(['/wallet']);
          window.location.href = url;
        })
        .catch(error => {
          this.alert.errorAlert(error);
        })
      .catch(error => {
        this.alert.errorAlert(error);
      });
    });
  });
}

  mockData() {
    this.chargesList = [
      {
        info: false,
        'state': {
          'data': {
            'directPayment': {
              'receiverOrg': 'O=OrgA, L=São Paulo, C=BR',
              'receiverAccount': '16c046a6-e3d9-462d-bd26-409651c127ba',
              'receiverName': 'Vitor',
              'receiverDocument': '123.123.123-12',
              'payerOrg': 'O=OrgA, L=São Paulo, C=BR',
              'payerAccount': 'a328d352-80dc-4fa9-80e5-a5fb8ffe86fd',
              'payerName': 'Joao',
              'payerDocument': '123.123.123-12',
              'createTime': '2019-02-28T14:40:05.628Z',
              'amount': 100.15,
              'status': 'PAID_OUT'
            },
            'linearId': {
              'externalId': null,
              'id': '0144b057-2bc8-487d-b141-bdf48670a523'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgA, L=São Paulo, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.DirectPaymentContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': 'AF9BE6D9C463B2CC7A81E9A9BA8FCDF92233D5F0BCBA7F7579BCCF47FB367552'
          }
        },
        'ref': {
          'txhash': 'AF906A62278128BBD2740A26CA643508B1424E059EBBAF08C31624FA15B0ECEA',
          'index': 0
        }
      },
      {
        info: false,
        'state': {
          'data': {
            'directPayment': {
              'receiverOrg': 'O=OrgB, L=São Paulo, C=BR',
              'receiverAccount': '16c046a6-e3d9-462d-bd26-409651c127ba',
              'receiverName': 'Vitor',
              'receiverDocument': '123.123.123-12',
              'payerOrg': 'O=OrgA, L=São Paulo, C=BR',
              'payerAccount': 'a328d352-80dc-4fa9-80e5-a5fb8ffe86fd',
              'payerName': 'Joao',
              'payerDocument': '123.123.123-12',
              'createTime': '2019-02-28T17:02:02.145Z',
              'amount': 100,
              'status': 'PENDING'
            },
            'linearId': {
              'externalId': null,
              'id': 'b38e31e4-be76-4dfe-a608-ea4c7869be7b'
            },
            'participants': [
              'O=OrgA, L=São Paulo, C=BR',
              'O=OrgA, L=São Paulo, C=BR'
            ]
          },
          'contract': 'tech.bluchain.democordanodes.contract.DirectPaymentContract',
          'notary': 'O=Notary, L=Campinas, C=BR',
          'encumbrance': null,
          'constraint': {
            'attachmentId': 'AF9BE6D9C463B2CC7A81E9A9BA8FCDF92233D5F0BCBA7F7579BCCF47FB367552'
          }
        },
        'ref': {
          'txhash': 'F9790FFB4D6770E600A8C7CAC494CA65C12ACBF616E82E27E43461054C285505',
          'index': 0
        }
      },
    ];
  }

}
