import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceHeaderPage } from './balance-header.page';

describe('BalanceHeaderPage', () => {
  let component: BalanceHeaderPage;
  let fixture: ComponentFixture<BalanceHeaderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceHeaderPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceHeaderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
