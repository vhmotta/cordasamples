import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header-balance',
  templateUrl: './balance-header.component.html',
  styleUrls: ['./balance-header.component.scss'],
})
export class BalanceHeaderComponent implements OnInit {

  @Input() balance;
  @Input() showBackButton;
  totalBalance;


  constructor() { }

  ngOnInit() {}

}
