import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { BalanceHeaderComponent } from './balance-header/balance-header.component';

@NgModule({
  declarations: [BalanceHeaderComponent],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [BalanceHeaderComponent]
})
export class ComponentsModule { }
