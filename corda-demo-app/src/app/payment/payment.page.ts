import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ModalController, LoadingController} from '@ionic/angular';
import { QrReaderPage } from '../pages/modals/qr-reader/qr-reader.page';
import { CordaService } from '../providers/corda/corda.service';
import { Router } from '@angular/router';
import { PaguevelozService } from '../providers/pagueveloz/pagueveloz.service';
import { AlertsService } from '../providers/alerts/alerts.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  @Input() slip;

  paymentCode;
  hasReceivedData = false;
  transferLoading;
  user;
  operationType;

  step = 0;

  moment = moment;

  paymentSlipForm = new FormGroup({
    accountToName: new FormControl('', Validators.compose([Validators.required])),
    accountTo: new FormControl('', Validators.compose([Validators.required])),
    accountToId: new FormControl('', Validators.compose([Validators.required])),
    orgTo: new FormControl('', Validators.compose([Validators.required])),
    orgToName: new FormControl('', Validators.compose([Validators.required])),
    dueDate: new FormControl(''),
    paymentDate: new FormControl('', Validators.compose([Validators.required])),
    amount: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('[0-9]+[,][0-9]{1,3}'),
    ])),
    // TODO: adicionar validador para impedir data de pagamento posterior a data de vencimento
  });


  constructor(
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alert: AlertsService,
    public router: Router,
    public corda: CordaService,
    private pagueVeloz: PaguevelozService,
  ) { }

  ngOnInit() {
    this.genLoading();
    this.getUser();
    if (this.slip) {
      this.paymentCode = this.slip.codigoDeBarras;
    } else {
      this.openQrModal();
    }
  }

  setStep(step) {
    this.step = step;
  }

  async msgAlert(msg, header, page) {
    this.alert.navAlert(msg, header, page);
  }

  sendPayment() {
    console.log(this.paymentSlipForm.value);
    this.transferLoading.present();

    this.corda.payPaymentSlip(this.paymentCode, this.user.linearId, parseFloat(this.paymentSlipForm.value.amount.replace(',', '.')))
      .then(() => {
        this.pagueVeloz.payIfIsPagueVeloz(this.paymentCode)
          .then( resp => {
            if ( resp === 1 ) {
              console.log('Atualizado Pgv');
            } else { console.log('Não Atualizado'); }
          });

        this.transferLoading.dismiss();
        this.msgAlert('Pagamento Realizado', 'Sucesso', 'wallet');
        this.modalCtrl.dismiss();
      })
      .catch(error => {
        this.transferLoading.dismiss();
        this.msgAlert('Erro no Pagamento', 'Erro', 'wallet');
      });
  }

  async openQrModal() {
    const modal = await this.modalCtrl.create({
      component: QrReaderPage,
      componentProps: {
        hasBarcode: true
      }
    });
    modal.present();
    const { data } = await modal.onDidDismiss();
    console.log(data);
    // TODO: validar se dados do qr code são válidos
    if (data && data !== 'close') {
      this.paymentCode = data;
      this.hasReceivedData = true;
    } else if (data && data === 'close') {
      this.modalCtrl.dismiss();
    }
  }

  async genLoading() {
    this.transferLoading = await this.loadingCtrl.create({
      message: 'Realizando transferência'
    });
  }

  mockPayment() {
    this.paymentSlipForm.patchValue({
      accountTo: '4a5d0a1c-2a68-46e7-ad03-6f58d3d91a26',
      accountToId: '123.123.123-12',
      accountToName: 'Vivo Telecom',
      amount: '123,12',
      dueDate: '2019-03-25',
      orgTo: 'OrgT',
      orgToName: 'VIVO',
    });
    this.paymentCode = 23793381286000403036153000063304478250000000300;
  }

  getUser() {
    this.user = JSON.parse(localStorage.getItem('cordaUser.'));
  }

  parseAmountTx(amount): number {
    const parsedAmount = parseFloat(amount.replace(',', '.'));
    return parsedAmount;
  }

  identifyCode() {
    console.log('id code');
    console.log(this.paymentCode);
    if (this.paymentCode.charAt(0) === '8') {
      this.applyConvenio();
      this.operationType = 'convenio';
    } else {
      this.applyBoleto();
      this.operationType = 'boleto';
    }
  }

  // 01691785300000012500002001007014900000153138
  applyBoleto() {

    console.log('apply boleto called');
    const boleto = this.paymentCode;
    const days = +boleto.slice(5, 9) + 1;

    const data = {
      'bank': this.bank(boleto.slice(0, 3)),
      'amount': boleto.slice(9, 19).replace(/^[0]+/g, '0'),
      'currency': boleto.slice(3, 4),
      'expirationDate': moment(this.expirationDate(days)).format('YYYY-MM-DD')
    };

    this.patchForm(data);

  }

  // 34191093210173406044340006400002578170000008360
  applyDigitavelBoleto() {

    console.log('apply digitavel called');
    const boleto = this.paymentCode;
    const days = +boleto.slice(33, 37) + 1;
    console.log('days:', days);

    const data = {
      'bank': this.bank(boleto.slice(0, 3)),
      'amount': boleto.slice(37).replace(/^[0]+/g, '0'),
      'currency': boleto.slice(3, 4),
      'expirationDate': moment(this.expirationDate(days)).format('YYYY-MM-DD')
    };

    this.patchForm(data);
  }

  // 836000000015101500403007989042227037100852783586
  applyConvenio() {
    console.log('apply convenio called');
    // tslint:disable-next-line:max-line-length
    const boleto = this.paymentCode.slice(0, 11) + this.paymentCode.slice(12, 23) + this.paymentCode.slice(24, 35) + this.paymentCode.slice(36, -1);
    console.log(boleto);

    const data = {
      'bank': this.bank(boleto.slice(15, 19)), // TODO: verificar codigos de concessionárias
      'amount': boleto.slice(4, 15).replace(/^[0]+/g, '0'),
    };
    console.log(data);
    this.patchForm(data);

  }

  patchForm(data) {
    this.paymentSlipForm.patchValue({
      accountTo: '4a5d0a1c-2a68-46e7-ad03-6f58d3d91a26', // TODO: definir organização recebedora de boletos no Corda
      accountToId: '123.123.123-12', // MockData
      accountToName: 'Vivo Telecom', // MockData
      amount: data.amount,
      dueDate: data.expirationDate,
      orgTo: 'OrgT', // TODO: definir organização recebedora de boletos no Corda
      orgToName: (this.slip ? this.slip.issuer.toUpperCase() : data.bank),
    });
  }

  expirationDate(days) {
    const refDate = new Date('1997-10-07');
    return new Date(refDate.getTime() + days * 86400000);
  }


  bank(bank) {
    // TODO: adicionar banco PagueVeloz
    switch (bank) {
      case '001': return 'Banco do Brasil';
      case '007': return 'BNDES';
      case '033': return 'Santander';
      case '069': return 'Crefisa';
      case '077': return 'Banco Inter';
      case '102': return 'XP Investimentos';
      case '104': return 'Caixa Econômica Federal';
      case '140': return 'Easynvest';
      case '197': return 'Stone';
      case '208': return 'BTG Pactual';
      case '212': return 'Banco Original';
      case '237': return 'Bradesco';
      case '260': return 'Nu Pagamentos';
      case '341': return 'Itaú';
      case '389': return 'Banco Mercantil do Brasil';
      case '422': return 'Banco Safra';
      case '505': return 'Credit Suisse';
      case '633': return 'Banco Rendimento';
      case '652': return 'Itaú Unibanco';
      case '735': return 'Banco Neon';
      case '739': return 'Banco Cetelem';
      case '745': return 'Citibank';
      case '016': return 'Pague Veloz';
      case '0040': return 'CPFL'; // TODO: verificar codigos de concessionárias
      default: return 'Unknown';
    }
  }

  dismissModal() {
    if (this.slip) {
      this.modalCtrl.dismiss();
    } else {
      this.router.navigate(['/wallet']);
    }
  }






}
