import { Component } from '@angular/core';
import { CordaService } from 'src/app/providers/corda/corda.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private corda: CordaService) {

    /*
this.receiveTransfer().then(receiverId => {
 console.log(receiverId);
  this.sendTransfer(receiverId).then(output => {
      this.getReceiverBalance(receiverId).then(balance => {
          console.log(balance);
      });
  });
});
*/


  }


  /*
  receiveTransfer() {
    return new Promise((resolve, reject) => {
      this.corda.setOrganization({host: 'https://corda.bluchain.tech:12003', org: 'OrgB', name: 'CEDWARD'})
          .then( () => {
        this.corda.createUserAccount('receivetransfer2', 10)
            .then((linearId: any) => {
            return resolve(linearId);
        })
            .catch(error => {
              return reject(error);
            });
      })
          .catch( error => {
            return reject(error);
          });
    });

  }


  sendTransfer(receiverId) {
    return new Promise((resolve, reject) => {
      this.corda.setOrganization({host: 'https://corda.bluchain.tech:12002', org: 'OrgB', name: 'CEDWARD'})
          .then( () => {
        this.corda.createUserAccount('sendtransfer2', 100)
            .then((linearId: any) => {

              console.log(linearId);

          this.corda.makeTransfer(linearId, 'OrgB', receiverId, 'sendtransfer2', 100)
              .then(output => {

            const data = {
              account: (output[0].data.account) ? output[0].data.account : output[1].data.account,
              transfer: (output[0].data.transfer) ? output[0].data.transfer : output[1].data.transfer
            };

            console.log(data);

            return resolve(data);
          });
        });
      });

    });


  }

    getReceiverBalance(receiverId) {
        return new Promise((resolve, reject) => {
            this.corda.setOrganization({host: 'https://corda.bluchain.tech:12003', org: 'OrgB', name: 'CEDWARD'})
                .then( () => {
                    this.corda.getBalanceById(receiverId)
                        .then((balance: any) => {
                            return resolve(balance);
                        })
                        .catch(error => {
                            return reject(error);
                        });
                })
                .catch( error => {
                    return reject(error);
                });
        });

    }

    */


}
