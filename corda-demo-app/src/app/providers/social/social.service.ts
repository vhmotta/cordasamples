import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as $ from 'jquery';


@Injectable({
  providedIn: 'root'
})
export class SocialService {

  constructor() { }

  whatsappShare(message) {
    return new Promise((resolve) => {
      const url = 'whatsapp://send?text=' + message;
      return resolve(url);
    });
  }

  whatsappShareRequest(user, amount, type) {
    return new Promise((resolve) => {
      // console.log(user);
      console.log();

      let urlParams = {
        subdom: '',
        type: type,
        accountTo: user.linearId,
        accountToName: user.name.replace(' ', '%20'),
        amount: amount.replace(',', '.'),
        orgTo: user.orgName,
        orgToName: environment.organization.name.replace(' ', '%20')
      };

      let isLocal = false;

      if (window.location.hostname.substring(0, 3) === '192' || '172' || '127' || '10.' || 'loc') {
        isLocal = true;
      }

      if (!isLocal) {
        if (urlParams.orgTo === 'orgA') {
          urlParams.subdom = window.location.protocol + '//' + environment.orgHostNames.orgA;
          console.log(urlParams.subdom);
        }
        if (urlParams.orgTo === 'orgB') {
          urlParams.subdom = window.location.protocol + '//' + environment.orgHostNames.orgB;
          console.log(urlParams.subdom);
        }
      } else {
        urlParams.subdom = window.location.protocol + '//' + window.location.hostname /*+ ':' + window.location.port*/;
        console.log(urlParams.subdom);
      }

      // tslint:disable-next-line:max-line-length
      const url = urlParams.subdom + '/transfer?type=' + urlParams.type + '&accountTo=' + urlParams.accountTo + '&accountToName=' + urlParams.accountToName + '&amount=' + urlParams.amount + '&orgTo=' + urlParams.orgTo + '&orgToName=' + urlParams.orgToName;

      // tslint:disable-next-line:max-line-length
      // const url = 'http://localhost:8100/transfer?type=' + type + 'accountTo=' + user.linearId + '&accountToName=' + user.name + '&amount=' + amount + '&orgTo=' + user.orgName + '&orgToName=' + environment.organization.name;
      // console.log(url);
      return resolve(url);
    });
  }

  whatsappShareCharge(user, rqLinearId, amount, type) {
    return new Promise((resolve) => {

      const userEncoded = user.name;
      const urlParams = {
        subdom: environment.organization.name.toLowerCase(), // TODO: criar subdomain no nginx programaticamente
        type: type,
        accountTo: rqLinearId,
        accountToName: user.name.replace(' ', '%20'),
        amount: amount,
        orgTo: user.orgName,
        orgToName: environment.organization.name.replace(' ', '%20')
      };

      if (window.location.hostname === 'localhost' || '192.168.1.126' || '192.168.1.53') {
        urlParams.subdom = window.location.protocol + '//' + window.location.hostname /*+ ':' + window.location.port*/;
      } else {
        urlParams.subdom = window.location.protocol + '//' + window.location.hostname;
      }

      // tslint:disable-next-line:max-line-length
      const url = urlParams.subdom + '/transfer?type=' + urlParams.type + '&accountTo=' + urlParams.accountTo + '&accountToName=' + urlParams.accountToName + '&amount=' + urlParams.amount + '&orgTo=' + urlParams.orgTo + '&orgToName=' + urlParams.orgToName;

      // tslint:disable-next-line:max-line-length
      // const url = 'http://localhost:8100/transfer?type=' + type + 'accountTo=' + rqLinearId + '&accountToName=' + user.name + '&amount=' + amount + '&orgTo=' + user.orgName + '&orgToName=' + environment.organization.name;

      return resolve(url);
    });
  }

  // Auto short an URL
  shotnerUrl(url) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: 'https://api.rebrandly.com/v1/links',
        type: 'post',
        data: JSON.stringify({
          'destination': url
          , 'domain': { 'fullName': 'rebrand.ly' }
          // , "slashtag": "A_NEW_SLASHTAG"
          // , "title": "Rebrandly YouTube channel"
        }),
        headers: {
          'Content-Type': 'application/json',
          'apikey': '62290b00bba04f2d92cbb0b7c0eee01e',
          'workspace': '0cdb357be57e4d4a8eb125222de49608'
        },
        dataType: 'json',
        success: function (link) {
          console.log('Long URL was ' + link.destination + ', short URL is ' + link.shortUrl);
          return resolve(link.shortUrl);
        }
      }).fail(function (error) {
        return reject(error);
      });

    });

  }


}
