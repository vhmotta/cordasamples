import { Injectable } from '@angular/core';
import * as $ from 'jquery';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PaguevelozService {

  constructor() { }

  getAllBoletos(token) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: 'https://api.blupay.orion.tech/boleto',
        type: 'GET',
        crossDomain: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token,
        },
        'processData': false,
        'data': '',
        success: function (response) {
          return resolve(response);
        }
      }).fail(function (error) {
        return reject(error);
      });
    });
  }

  updateStatusBoleto(boletoId, token) {
    return new Promise((resolve, reject) => {
      const data = {
        'id': boletoId,
        'status': 1
      };

      $.ajax({
        url: 'https://api.blupay.orion.tech/boleto/updateStatus',
        type: 'POST',
        crossDomain: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token,
        },
        'processData': false,
        'data': JSON.stringify(data),
        success: function (response) {
          return resolve(response);
        }
      }).fail(function (error) {
        return reject(error);
      });
    });
  }

  createBoleto(data, token) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: 'https://api.blupay.orion.tech/boleto',
        type: 'POST',
        crossDomain: true,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Content-Type',
          'Authorization': token,
        },
        'processData': false,
        'data': JSON.stringify(data),
        success: function (response) {
          return resolve(response);
        }
      }).fail(function (error) {
        return reject(error);
      });
    });
  }

  getMyBoletoId(myCode) {
    return new Promise((resolve) => {
      let result;
      environment.tokens.forEach(el => {
        this.getAllBoletos(el.token)
          .then((boletos: Array<any>) => {
            for (let i = 0; i < boletos.length; i++) {
              if (boletos[i].codigoDeBarras === myCode) {
                result = {
                  'id': boletos[i].id,
                  'token': el.token
                };
                return resolve(result);
              }
            }
          });
      });
    });
  }


  getMyBoletoDocument(document, token) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: 'https://api.blupay.orion.tech/boleto?status=4&documento=' + document + '&incluirCancelado=false',
        type: 'GET',
        crossDomain: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token,
        },
        'processData': false,
        'data': '',
        success: function (response) {
          return resolve(response);
        }
      }).fail(function (error) {
        return reject(error);
      });
    });
  }

  payIfIsPagueVeloz(myCode) {
    return new Promise((resolve) => {
      this.getMyBoletoId(myCode).then((boleto: any) => {
        this.updateStatusBoleto(boleto.id, boleto.token)
          .then(res => {
            return resolve(1);
          })
          .catch(res => {
            return resolve(0);
          });
      });
    });
  }


}


