import { Injectable } from '@angular/core';
import {AlertController, LoadingController, ModalController} from '@ionic/angular';
import { Router } from '@angular/router';
import { SocialService } from '../social/social.service';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {
  createLoading;

  constructor(
    private router: Router,
    private loadingCtrl: LoadingController,
    private  alertController: AlertController,
    private modalCtrl: ModalController,
    private social: SocialService
    ) { }

    async genLoading(msg) {
      return await this.loadingCtrl.create({
        message: msg
      });
    }

    async errorAlert(error) {
      const alert = await this.alertController.create({
        header: 'Erro',
        message: error,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Okay');
                this.modalCtrl.dismiss();
              }
            }
        ]
      });

      await alert.present();
    }

    async navAlert(msg, header, page?) {
      const alert = await this.alertController.create({
        header: header,
        message: msg,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Okay');
              if (page) {
                this.modalCtrl.dismiss();
                this.router.navigate(['/' + page]);
              }
            }
          }
        ]
      });

      await alert.present();
    }

    async successWaAlert(header, message, page, share) {
      const alert = await this.alertController.create({
        header: header,
        message: message, // TODO Copywrite - Catarina
        buttons: [
          {
            text: 'Ok',
            role: 'ok',
            cssClass: 'secondary',
            handler: () => {
              this.modalCtrl.dismiss();
              this.router.navigate(['/' + page]);
            }
          },
          {
            text: 'Compartilhar',
            role: 'compartilhar',
            cssClass: 'secondary',
            handler: () => {
              this.modalCtrl.dismiss();
              this.router.navigate(['/' + page]);
              this.social.whatsappShare(share)
              .then((url: string) => {
                window.location.href = url;
              });
            }
          }
        ]
      });
      await alert.present();
    }

    async successAlert(header, message, page, share) {
      const alert = await this.alertController.create({
        header: header,
        message: message, // TODO Copywrite - Catarina
        buttons: [
          {
            text: 'Ok',
            role: 'ok',
            cssClass: 'secondary',
            handler: () => {
              this.modalCtrl.dismiss();
              this.router.navigate(['/' + page]);
            }
          }
        ]
      });
      await alert.present();
    }


    async successAlertModal(header, message, page) {
      const alert = await this.alertController.create({
        header: header,
        message: message, // TODO Copywrite - Catarina
        buttons: [
          {
            text: 'Ok',
            role: 'ok',
            cssClass: 'secondary',
            handler: () => {
              this.modalCtrl.dismiss();
              this.router.navigate(['/' + page]);
            }
          },
          {
            text: 'Compartilhar',
            role: 'compartilhar',
            cssClass: 'secondary',
            handler: () => {
              this.modalCtrl.dismiss();
              this.router.navigate(['/' + page]);
              this.social.whatsappShare('Sua cobrança foi paga, verifique sua conta.')
              .then((url: string) => {
                window.location.href = url;
              });            }
          }
        ]
      });
      await alert.present();
    }

    async infoAlert(info, infoHeader) {
      const alert = await this.alertController.create({
        header: infoHeader,
        message: info,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              console.log('Confirm Okay');
                this.modalCtrl.dismiss();
              }
            }
        ]
      });

      await alert.present();
    }


}
