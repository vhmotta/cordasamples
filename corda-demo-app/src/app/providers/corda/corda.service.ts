import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as firebase from 'firebase';
import * as $ from 'jquery';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class CordaService {

    balance;
    userData;
    orgHost;
    isLocal = false;
    orgData;


    constructor() {
        /*
            const config = environment.firebase;
            firebase.initializeApp(config);
        */


        if (window.location.hostname.substring(0, 3) === '192' || '172' || '127' || '10.' || 'loc') {
            this.isLocal = true;
        }


        if (!this.isLocal) {
            environment.hosts.forEach(host => {
                if (window.location.hostname.split('.')[0] = host.hostName) {
                    this.orgHost = host.hostApi;
                    this.orgData = host;
                }
            });
        } else {
            this.orgHost = environment.hosts[0].hostApi;
            this.orgData = environment.hosts[0];
        }


        this.getbalance();

    }



    // *** Metodos Executivos *** //
    /*
        initNodeApp() {
            return new Promise((resolve, reject) => {

                // Descobre qual a url usada e define qual o host vai ser aplicado, retorna org
                const stp1 = window.location.href.split(':');
                const hostPort = stp1[2].split('/')[0];

                // Busca uma lista das organizações
                this.getMyOrganizations(hostPort)
                    .then((org: any) => {
                        // Define qual host a aplicação vai usar
                        this.setOrganization(org)
                            .then(() => {
                                return resolve(environment.organization);
                            })
                            .catch(err => {
                                return reject(err);
                            });
                    })
                    .catch(err => {
                        return reject(err);
                    });
            });
        }
    */

    getbalance() {
        if (this.hasLocalUser()) {
            this.getUserBalance().then(balance => {
                this.balance = balance;
            });

            setInterval(() => {
                this.getUserBalance().then(balance => {
                    this.balance = balance;
                });
            }, 3000);
        }
    }
    // Checa se existe um usuário no storage location
    hasLocalUser() {
        return !!localStorage.getItem('cordaUser.');
    }

    // Cria um novo usuário no node da organização
    createUserAccount(userName, balance, document) {
        return new Promise((resolve, reject) => {
            this.createNewUser(userName, balance, document)
                .then((resp: any) => {

                    const data = {
                        orgName: resp.data.account.orgName,
                        name: resp.data.account.name,
                        linearId: resp.data.linearId.id,
                        document: document
                    };

                    console.log(data);

                    localStorage.setItem('cordaUser.', JSON.stringify(data));
                    return resolve(data.linearId);
                })
                .catch(error => {
                    return reject(error);
                });
        });
    }

    // busca o balance do usuário
    getUserBalance() {
        return new Promise((resolve, reject) => {
            const user: any = JSON.parse(localStorage.getItem('cordaUser.'));
            this.getBalanceById(user.linearId)
                .then((resp: any) => {
                    this.userData = resp.state.data;
                    const balance = resp.state.data.account.balance;
                    //      console.log(resp.state.data);
                    return resolve(balance);
                })
                .catch(err => {
                    return reject(err);
                });
        });
    }
    // TODO: adicionar histórico de pagamento de cobranças
    getHistory(linearId) {
        return new Promise((resolve, reject) => {
            let history = [];
            // Busca histórico de transferências
            this.getTxHistory(linearId).then((transfers: Array<any>) => {
                // console.log(transfers);
                transfers.forEach(transfer => {
                    history.push(Object.assign(
                        {
                            info: false,
                            type: 'transfer',
                            timestamp: transfer.state.data.transfer.createTime
                        },
                        transfer));
                });
                // Busca histórico de pagamento de boletos
                this.getPaymentHistory(linearId).then((payments: Array<any>) => {
                    // console.log(payments);
                    payments.forEach(payment => {
                        history.push(Object.assign(
                            {
                                info: false,
                                type: 'payment',
                                timestamp: payment.state.data.paymentSlip.createTime
                            },
                            payment));
                    });

                    this.directPaymentHistory(linearId).then((charges: Array<any>) => {
                        // console.log(charges);
                        charges.forEach(charge => {
                            // TODO: refatorar no Corda para enviar apenas estado mais recente da cobrança
                            if (charge.state.data.directPayment.status === 'PAID_OUT') {

                                history.push(Object.assign(
                                    {
                                        info: false,
                                        type: 'charge',
                                        timestamp: charge.state.data.directPayment.createTime
                                    },
                                    charge));
                            }

                        });
                        // console.log(history);

                        // Ordena o histórico do mais recente para mais antigo
                        history = history.sort((a, b) => {
                            const timeA = new Date(a.timestamp);
                            const timeB = new Date(b.timestamp);
                            if (timeA < timeB) { return 1; }
                            if (timeA > timeB) { return -1; }
                            return 0;
                        });
                        return resolve(history);
                    });

                });
            })
                .catch(err => {
                    return reject(err);
                });
        });


    }


    // *** Metodos Auxiliares *** //

    // Define qual a organização que o app fará leitura e gravação
    setOrganization(org) {
        return Promise.resolve(org).then(() => {
            environment.organization = org;
            return 'success';
        });
    }

    get(endpoint: string, data: any) {
        return new Promise((resolve, reject) => {
            endpoint = this.orgHost + endpoint;

            $.ajax({
                url: endpoint,
                type: 'GET',
                crossDomain: true,
                headers: {
                    'content-type': 'application/json',
                },
                data: JSON.stringify(data),
                success: function (response) {
                    return resolve(response);
                }
            }).fail(function (jqXHR, textStatus, err) {
                console.log('Status: ', textStatus);
                console.log('Error: ', err);
                console.log('Message: ', jqXHR.responseText);
                return reject(jqXHR.responseText);
            });
        });
    }

    put(endpoint: string, data: any) {
        return new Promise((resolve, reject) => {
            endpoint = this.orgHost + endpoint;

            $.ajax({
                url: endpoint,
                type: 'PUT',
                crossDomain: true,
                headers: {
                    'content-type': 'application/json',
                },
                data: JSON.stringify(data),
                success: function (response) {
                    return resolve(response);
                }
            }).fail(function (jqXHR, textStatus, err) {
                console.log('Status: ', textStatus);
                console.log('Error: ', err);
                console.log('Message: ', jqXHR.responseText);
                let message: string = jqXHR.responseText;
                if (message.indexOf('Failed requirement:') >= 0) {
                    // tslint:disable-next-line:max-line-length
                    message = message.substring(message.indexOf('Failed requirement:') + 20, message.indexOf(',', message.indexOf('Failed requirement:')));
                }
                return reject(message);
            });
        });
    }

    // Account create - Cria um novo usuário no node da organização do usuário
    createNewUser(userName: string, balance: number, document: string) {
        return new Promise((resolve, reject) => {
            const data = {
                name: userName,
                balance: balance,
                document: document
            };

            this.put('/api/account/create', data)
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }


    // Account users - Obtem todos os usuários
    getAllUsers() {
        return new Promise((resolve, reject) => {
            this.get('/api/account/users', '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // Account user by document - Busca um usuário pelo num. do documento
    getUserByDocument(document: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/account/user-by-document?document=' + document, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // Account user - Busca o balance de um determinado usuário
    getBalanceById(linearId: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/account/user?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }
    /*
        // Busca todas as organizações no banco de dados
        getAllOrganizations() {
            return new Promise((resolve, reject) => {
                firebase
                    .database()
                    .ref('Organizations/')
                    .on('value', snapshot => {
                        if (snapshot) {
                            return resolve(snapshot.val());
                        } else {
                            return reject();
                        }
                    });
            });
        }

        // Busca todas as organizações no banco de dados
        getMyOrganizations(id) {
            return new Promise((resolve, reject) => {
                firebase
                    .database()
                    .ref('Organizations/' + id)
                    .on('value', snapshot => {
                        if (snapshot) {
                            return resolve(snapshot.val());
                        } else {
                            return reject();
                        }
                    });
            });
        }
    */
    // Account Tx Track
    getTxTrack(txid: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/account/tx-track?txid=', '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // Account Tx Track
    getAccountTrack(txid: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/account/account-track?id=', '')
                .then(result => {
                    return resolve(result);
                })
                .catch(error => {
                    return reject(error);
                    console.log(error);
                });
        });
    }

    // Account All
    getAccountAll() {
        return new Promise((resolve, reject) => {
            this.get('/api/account/all', '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // Transfer Transfer - Executa uma transferencia entre organizações e LinearIds
    makeTransfer(from: string, orgTo: string, to: string, nameTo: string, amount: number) {
        return new Promise((resolve, reject) => {

            const data = {
                from: from,
                orgTo: orgTo,
                to: to,
                amount: amount
            };

            this.put('/api/transfer/transfer', data)
                .then(result => resolve(result))
                .catch(error => reject(error));
        });

    }

    // Transfer all - Busca todos os states de transferencia compartilhados com o node solicitante
    getAllTransfer() {
        return new Promise((resolve, reject) => {
            this.get('/api/transfer/all', {})
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // Transfer history -  Busca o histórico de states de transações de um linearId
    getTxHistory(linearId: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/transfer/history?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // Transfer history-to - Busca o histórico de states de transações enviadas para um linearId
    getTxHistoryTo(linearId: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/transfer/history-to?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // Transfer history-from - Busca o histórico de states de transações recebidas para um linearId
    getTxHistoryFrom(linearId: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/transfer/history-from?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    getMyOrg(linearId: string) {
        return environment.organization;
    }


    // *** Metodos Pagamento de Boleto *** //

    // Paymentslip all - Traz os estados não consumidos
    getPaymentSlip() {
        return new Promise((resolve, reject) => {
            this.get('/api/paymentslip/get', '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // PaymentSlip pay
    payPaymentSlip(code, account, amount) {
        return new Promise((resolve, reject) => {

            const data = {
                code: code,
                accountId: account,
                amount: amount
            };

            this.put('/api/paymentslip/pay', data)
                .then(result => resolve(result))
                .catch(error => reject(error));
        });

    }

    // PaymentSlip pay
    payPaymentDarf(code, account, amount) {
        return new Promise((resolve, reject) => {

            const data = {
                code: code,
                accountId: account, // account do governo
                amount: amount
            };

            this.put('/api/paymentslip/pay', data)
                .then(result => resolve(result))
                .catch(error => reject(error));
        });

    }

    // PaymentSlip history
    getPaymentHistory(linearId) {
        return new Promise((resolve, reject) => {
            this.get('/api/paymentslip/history?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // PaymentSlip all
    getAllPaymentSlip() {
        return new Promise((resolve, reject) => {
            this.get('/api/paymentslip/all', '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // *** Metodos Cobrança Direta *** //

    // DirectPayment create
    directPaymentCreate(charge) {
        return new Promise((resolve, reject) => {

            const data = {
                receiverAccount: charge.receiverAccount,
                payerOrg: charge.payerOrg,
                payerDocument: charge.payerDocument,
                amount: charge.amount
            };

            this.put('/api/directpayment/create', data)
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // DirectPayment pay
    directPaymentPay(payerAccountId, paymentId) {
        return new Promise((resolve, reject) => {

            const data = {
                payerAccountId: payerAccountId,
                paymentId: paymentId,
            };

            this.put('/api/directpayment/pay', data)
                .then(result => resolve(result))
                .catch(error => reject(error));
        });
    }

    // DirectPayment to-receive
    directPaymentToReceive(linearId: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/directpayment/to-receive?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });

    }

    // DirectPayment to-pay
    directPaymentToPay(linearId: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/directpayment/to-pay?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });

    }

    // DirectPayment history
    directPaymentHistory(linearId: string) {
        return new Promise((resolve, reject) => {
            this.get('/api/directpayment/history?id=' + linearId, '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });

    }

    // DirectPayment all
    directPaymentAll() {
        return new Promise((resolve, reject) => {
            this.get('/api/paymentslip/all', '')
                .then(result => resolve(result))
                .catch(error => reject(error));
        });

    }

    createGovAccount() {
        return new Promise((resolve, reject) => {

            const data = {
                name: 'Government',
                balance: 1,
                document: environment.governmentId
            };

            $.ajax({
                url: 'https://corda.bluchain.tech:13002/api/account/create',
                type: 'PUT',
                crossDomain: true,
                headers: {
                    'content-type': 'application/json',
                },
                data: JSON.stringify(data),
                success: function (response) {
                    return resolve(response);
                }
            }).fail(function (jqXHR, textStatus, err) {
                console.log('Status: ', textStatus);
                console.log('Error: ', err);
                console.log('Message: ', jqXHR.responseText);
                let message: string = jqXHR.responseText;
                if (message.indexOf('Failed requirement:') >= 0) {
                    // tslint:disable-next-line:max-line-length
                    message = message.substring(message.indexOf('Failed requirement:') + 20, message.indexOf(',', message.indexOf('Failed requirement:')));
                }
                return reject(message);
            });
        });

    }

    // Create a gov account if it does not exists in orgA
    checkGovAccount() {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'https://corda.bluchain.tech:13002/api/account/user-by-document?document=' + environment.governmentId,
                type: 'GET',
                crossDomain: true,
                headers: {
                    'content-type': 'application/json',
                },
                data: '',
                success: function (response) {
                    return resolve(response);
                }
            }).fail(function (jqXHR, textStatus, err) {
                console.log('Status: ', textStatus);
                console.log('Error: ', err);
                console.log('Message: ', jqXHR.responseText);
                return reject(jqXHR.responseText);
            });
        });

    }

    // Pays a tax to the gov account
    payGovernmentTax(from: string, amount: number) {
        return new Promise((resolve, reject) => {
            this.checkGovAccount()
                .then((resp: any) => {

                    const data = {
                        from: from,
                        orgTo: 'OrgA',
                        to: resp.state.data.linearId.id,
                        amount: amount
                    };

                    this.put('/api/transfer/transfer', data)
                        .then(result => resolve(result))
                        .catch(error => reject(error));
                });
        });

    }

}
