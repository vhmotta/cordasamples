// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
  {
    apiKey: 'AIzaSyDEPC5kOMMK7JwtkficCyQCIQ6VAqITXFk',
    authDomain: 'democorda.firebaseapp.com',
    databaseURL: 'https://democorda.firebaseio.com',
    projectId: 'democorda',
    storageBucket: 'democorda.appspot.com',
    messagingSenderId: '353270412309'
  },
  organization: {

    // TODO: apontar host de OrgC para url do terceiro nó

    // TODO: Os nomes das Orgs não podem possuir espaços, refatorar implementação de socialshare para tratar as strings

    // host: 'https://corda.bluchain.tech:13004',
    // org: 'OrgC',
    // name: 'IF3', // RTM
    // theme: 'vermelho'

    // host: 'https://corda.bluchain.tech:13002',
    // org: 'OrgA',
    // name: 'Bradesco', // SEM PARAR
    // theme: 'azul'

    host: 'https://corda.bluchain.tech:13003',
    org: 'OrgB',
    name: 'Itau', // VIVO
    theme: 'azul'
  },

  // TODO: Os nomes das Orgs não podem possuir espaços, refatorar implementação de socialshare para tratar as strings
  orgList: {
    'O=OrgA, L=São Paulo, C=BR': 'Bradesco',
    'O=OrgB, L=Campinas, C=BR': 'Itau',
    'O=OrgC, L=São Paulo, C=BR': 'IF3',
  },

  orgHostNames: {
    orgA: 'bradesco.bluchain.tech',
    orgB: 'itau.bluchain.tech'
  },

  hosts:
    [
      // {
      //   hostApi: 'https://corda.bluchain.tech:13002',
      //   hostName: 'brasdesco',
      //   org: 'OrgA',
      //   cordaName: 'O=OrgA, L=São Paulo, C=BR',
      //   name: 'Pague Veloz', // SEM PARAR
      //   theme: 'azul'
      //  }
      {
        hostApi: 'https://corda.bluchain.tech:13003',
        hostName: 'itau',
        org: 'OrgB',
        cordaName: 'O=OrgB, L=Campinas, C=BR',
        name: 'itau', // VIVO
        theme: 'azul'
      }
    ],

  tokens: [
    { name: 'net', token: 'Basic Y2xhcm9AY2xhcm8uY29tOmY2ZTA4ODg3LTIwNDAtNGIzZS04NjZjLWRmZjA0MjhmNDAyZQ==' },
    { name: 'vivo', token: 'Basic dml2b0B2aXZvLmNvbToxNTE0MDJhZS1iNWYwLTQwYTQtYWUxOS1iYzdjNzU3MjgzYmU=' },
    { name: 'eletropaulo', token: 'Basic ZWxldHJvcGF1bG9AZWxldHJvcGF1bG8uY29tOjRhMGM5YmU2LWFhZDktNDhhYi1hYWQzLTAwNjczZTU5NGNkYQ==' },
    { name: 'pague veloz', token: 'Basic Y29udGF0b0BibHVjaGFpbi50ZWNoOjhkOGJiOTUwLWRiNTUtNDUxMy05NzZkLWE4YmZkNmIxOTZkYw==' }
  ],

  governmentId: '639.312.080-01', // TODO: formatar cpf com mascara: '639.312.080-01'
  governmentLinearId: '',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
